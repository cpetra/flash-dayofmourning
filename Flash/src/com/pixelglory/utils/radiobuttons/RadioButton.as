package com.pixelglory.utils.radiobuttons
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class RadioButton extends Sprite
	{
		public static const UPDATED		: String = "UPDATED";
		
		public var bg					: MovieClip;
		public var selector				: MovieClip;
		
		private var _enabled			: Boolean = true;
		private var _selected			: Boolean = false;
		
		public function RadioButton()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			selector.visible = false;
			this.buttonMode = true;
			this.addEventListener(MouseEvent.CLICK, handleClick, false, 0, true);
		}
		
		private function handleClick( evt:MouseEvent ):void
		{
			if(!_selected){
				_selected = true;
				selector.visible = true;
				dispatchEvent(new Event(RadioButton.UPDATED));
			}else{
				// do nothing
			}
		}
		
		public function select():void
		{
			_selected = true;
			selector.visible = true;
			dispatchEvent(new Event(RadioButton.UPDATED));
		}
		
		public function deselect():void
		{
			_selected = false;
			selector.visible = false;
		}
		
		public function set disabled( pDisabled:Boolean ):void
		{
			if(pDisabled){
				this.removeEventListener(MouseEvent.CLICK, handleClick);
				this.buttonMode = false;
				this.alpha = 0.5;
			}else{
				this.buttonMode = true;
				this.addEventListener(MouseEvent.CLICK, handleClick, false, 0, true);
				this.alpha = 1;
				dispatchEvent(new Event(RadioButton.UPDATED));
			}
		}
		
	}
}