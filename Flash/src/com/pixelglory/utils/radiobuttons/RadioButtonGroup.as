package com.pixelglory.utils.radiobuttons
{
	import flash.events.Event;
	
	public class RadioButtonGroup
	{
		
		private var _buttonArray	: Array;
		private var _selectedButton	: RadioButton;
		
		public function RadioButtonGroup()
		{
			init();
		}
		
		private function init():void
		{
			_buttonArray = [];
		}
		
		public function subscribe( pButtons:Array, pDefault:RadioButton ):void
		{
			for(var i:int = 0; i< pButtons.length; i++){
				pButtons[i].addEventListener(RadioButton.UPDATED, handleUpdate, false, 0, true);
			}
			
			_buttonArray = _buttonArray.concat(pButtons);

			pDefault.select();
		}
		
		private function handleUpdate( evt:Event ):void
		{	
			for(var i:int = 0; i<_buttonArray.length; i++){
				if(_buttonArray[i] != evt.currentTarget){
					_buttonArray[i].deselect();
				}else{
					_selectedButton = evt.currentTarget as RadioButton;
				}
			}
		}
		
		public function get selected():RadioButton { return _selectedButton; };

	}
}