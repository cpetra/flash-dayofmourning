package com.pixelglory.utils.validation
{
	public class PhoneValidation
	{
		public function PhoneValidation()
		{
		}
		
		public static function check( pPhone:String ):Boolean
		{
			var emailExpression:RegExp = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/i;
			return ! emailExpression.test(pPhone);

		}

	}
}