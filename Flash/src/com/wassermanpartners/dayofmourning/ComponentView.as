package com.wassermanpartners.dayofmourning
{
	import com.wassermanpartners.dayofmourning.events.TransitionEvent;
	import com.wassermanpartners.dayofmourning.ScreenModel;
	import com.wassermanpartners.dayofmourning.ScreenController;
	
	import flash.display.MovieClip;
	import flash.errors.IllegalOperationError;
	import flash.events.Event;	

	public class ComponentView extends MovieClip
	{
		protected var model			: ScreenModel;
		protected var controller	: ScreenController;
		
		public function ComponentView( pModel:ScreenModel, pController:ScreenController = null )
		{
			super();
			this.model		= pModel;
			this.controller	= pController;
		}
		
		public function add( c:ComponentView ):void {
			throw new IllegalOperationError("add opperation no supported");
		}
		
		public function remove( c:ComponentView ):void {
			throw new IllegalOperationError("remove operation no supported");
		}
		
		public function getChild( n:int ):ComponentView {
			throw new IllegalOperationError("getChild operation not supported");
			return null;
		}
		
		//ABSTRACT Method (must be overridden in a subclass
		public function transitionInScreen( evt:TransitionEvent = null):void {}
		public function transitionOutScreen( evt:TransitionEvent = null):void {}
		
	}
}