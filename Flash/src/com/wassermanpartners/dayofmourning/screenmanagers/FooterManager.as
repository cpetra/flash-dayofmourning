package com.wassermanpartners.dayofmourning.screenmanagers
{
	import com.wassermanpartners.dayofmourning.ScreenController;
	import com.wassermanpartners.dayofmourning.ScreenModel;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	

	public class FooterManager extends MovieClip
	{
		private var _model		: ScreenModel;
		private var _controller	: ScreenController;
		
		public var terms		: MovieClip;
		public var privacy		: MovieClip;
		/*public var worksafe		: MovieClip;
		public var domSeal		: MovieClip;
		public var bcofbc		: MovieClip;
		public var bcfed		: MovieClip;*/
		public var wsForlifeLogoWhite		: MovieClip;
		public var nsGovtLogoWhite		: MovieClip;
		public var nsflLogoWhite		: MovieClip;
		public var domLogoWhite		: MovieClip;
		public var threadsOfLifeLogoWhite		: MovieClip;
		
		public function FooterManager( pModel:ScreenModel, pController:ScreenController )
		{
			super();
			_model = pModel;
			_controller = pController;
			
			init();
		}
		
		private function init():void {
			/*worksafe.buttonMode	= true;
			domSeal.buttonMode	= true;
			bcofbc.buttonMode	= true;
			bcfed.buttonMode 	= true;*/
			wsForlifeLogoWhite.buttonMode = true;
			nsGovtLogoWhite.buttonMode = true;
			nsflLogoWhite.buttonMode = true;
			domLogoWhite.buttonMode = true;
			threadsOfLifeLogoWhite.buttonMode = true;
			
			terms.addEventListener(MouseEvent.CLICK, handleTermsScreen, false, 0, true);
			privacy.addEventListener(MouseEvent.CLICK, handlePrivacyScreen, false, 0, true);
			/*worksafe.addEventListener(MouseEvent.CLICK, handleWorksafeClick, false, 0, true);
			domSeal.addEventListener(MouseEvent.CLICK, handleDOMSealClick, false, 0, true);
			bcofbc.addEventListener(MouseEvent.CLICK, handleBcofbcClick, false, 0, true);
			bcfed.addEventListener(MouseEvent.CLICK, handleBCFedClick, false, 0, true);*/
			wsForlifeLogoWhite.addEventListener(MouseEvent.CLICK, handleWSForLifeClick, false, 0, true);
			nsGovtLogoWhite.addEventListener(MouseEvent.CLICK, handleNSGovtClick, false, 0, true);
			nsflLogoWhite.addEventListener(MouseEvent.CLICK, handleNSFLClick, false, 0, true);
			domLogoWhite.addEventListener(MouseEvent.CLICK, handleDOMLogoClick, false, 0, true);
			threadsOfLifeLogoWhite.addEventListener(MouseEvent.CLICK, handleThreadsClick, false, 0, true);
		}
		
		private function handleTermsScreen( evt : MouseEvent ):void {
			_controller.holdHeadDedication("no name", "no flower id", false);
			_controller.changeScreenInit("TERMS_SCREEN");
		}
		
		private function handlePrivacyScreen( evt : MouseEvent ):void {
			_controller.holdHeadDedication("no name", "no flower id", false);
			_controller.changeScreenInit("PRIVACY_SCREEN");
		}
		/*
		private function handleWorksafeClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.worksafebc.com");
			navigateToURL(targetURL, "_blank"); 
		}
		
		private function handleDOMSealClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.worksafebc.com/news_room/campaigns/day_of_mourning/default.asp");
			navigateToURL(targetURL, "_blank"); 
		}
		
		private function handleBcofbcClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.bcbc.com");
			navigateToURL(targetURL, "_blank"); 
		}
		
		private function handleBCFedClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.bcfed.com");
			navigateToURL(targetURL, "_blank"); 
		} */
		private function handleWSForLifeClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.worksafeforlife.ca");
			navigateToURL(targetURL, "_blank"); 
		}
		private function handleNSGovtClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.gov.ns.ca");
			navigateToURL(targetURL, "_blank"); 
		}
		private function handleNSFLClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.nsfl.ns.ca");
			navigateToURL(targetURL, "_blank"); 
		}
		private function handleDOMLogoClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.dayofmourning.ns.ca");
			navigateToURL(targetURL, "_blank"); 
		}
		private function handleThreadsClick( evt : MouseEvent ):void {
			var targetURL:URLRequest = new URLRequest("http://www.threadsoflife.ca");
			navigateToURL(targetURL, "_blank"); 
		}
		
	}
}