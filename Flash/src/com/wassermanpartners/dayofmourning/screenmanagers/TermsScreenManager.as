package com.wassermanpartners.dayofmourning.screenmanagers
{
	import com.wassermanpartners.dayofmourning.ComponentView;
	import com.wassermanpartners.dayofmourning.ScreenController;
	import com.wassermanpartners.dayofmourning.ScreenModel;
	import com.wassermanpartners.dayofmourning.events.TransitionEvent;
	import com.wassermanpartners.dayofmourning.assets.GenericTextButton;
	
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	

	public class TermsScreenManager extends ComponentView
	{
		public static const SCREEN_NAME : String 		= "TERMS_SCREEN";
		
		public var close_btn			: GenericTextButton;
		public var upArrow				: MovieClip;
		public var downArrow			: MovieClip;
		public var txt					: MovieClip;
		public var scrollMask			: MovieClip;
		
		private var _scrollIncrement 	: Number = 100;
		private var _activeScreen		: Boolean = false;
		
		public function TermsScreenManager( pModel:ScreenModel, pController:ScreenController = null)
		{
			super(pModel, pController);
			this.visible 	= false;
			this.alpha 		= 0;
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0 , true);
		}
		
		private function init(evt:Event):void {
			trace("INTRO SCREEN ADDED TO STAGE AND INITED");
			
			// setup any assets and ready for trans in
			scrollMask.cacheAsBitmap	= true;
			txt.cacheAsBitmap 			= true;
			txt.mask					= scrollMask;
			
			if(txt.height < scrollMask.height){
				upArrow.visible = false;
				downArrow.visible = false;
			}
		}
		
		/*
		TTTTTT RRRRR     AAA   NN  NN  SSSSS         IIIIII NN  NN 
		  TT   RR  RR   AAAAA  NNN NN SS               II   NNN NN 
		  TT   RRRRR   AA   AA NNNNNN  SSSS            II   NNNNNN 
		  TT   RR  RR  AAAAAAA NN NNN     SS           II   NN NNN 
		  TT   RR   RR AA   AA NN  NN SSSSS          IIIIII NN  NN 
		*/
		override public function transitionInScreen(evt:TransitionEvent=null):void {
			if(evt.targetScreen == SCREEN_NAME){
				trace("TransIn " + SCREEN_NAME);
				this.visible = true;
				TweenMax.to(this, 0.8, {alpha:1, onComplete:transInComplete});
			}
		}
		
		private function transInComplete():void {
			trace("TransIn Complete");
			addListeners();
		}
		
		/*
		TTTTTT RRRRR     AAA   NN  NN  SSSSS          OOOO  UU   UU TTTTTT 
		  TT   RR  RR   AAAAA  NNN NN SS             OO  OO UU   UU   TT   
		  TT   RRRRR   AA   AA NNNNNN  SSSS          OO  OO UU   UU   TT   
		  TT   RR  RR  AAAAAAA NN NNN     SS         OO  OO UU   UU   TT   
		  TT   RR   RR AA   AA NN  NN SSSSS           OOOO   UUUUU    TT   
		*/
		override public function transitionOutScreen(evt:TransitionEvent=null):void {
			if(evt.targetScreen == SCREEN_NAME){
				// transition out code goes here. after it is complete run controller.transitionOutComplete
				trace("transitioning out "+ SCREEN_NAME);
				TweenMax.to(this, 1, {alpha:0, ease:Quad.easeOut, onComplete:transOutComplete});
				
			}
		}
		
		private function transOutComplete():void {
			_activeScreen	= false;
			this.visible 	= false;
			controller.transitionOutComplete();
		}
		
		/*
		  AAA   DDDDDD  DDDDDD          LL      IIIIII  SSSSS TTTTTT EEEEEEE NN  NN EEEEEEE RRRRR    SSSSS 
		 AAAAA  DD   DD DD   DD         LL        II   SS       TT   EE      NNN NN EE      RR  RR  SS     
		AA   AA DD   DD DD   DD         LL        II    SSSS    TT   EEEE    NNNNNN EEEE    RRRRR    SSSS  
		AAAAAAA DD   DD DD   DD         LL        II       SS   TT   EE      NN NNN EE      RR  RR      SS 
		AA   AA DDDDDD  DDDDDD          LLLLLLL IIIIII SSSSS    TT   EEEEEEE NN  NN EEEEEEE RR   RR SSSSS  
		*/
		private function addListeners():void {
			close_btn.addEventListener(MouseEvent.CLICK, handleClose, false, 0, true);
			upArrow.addEventListener(MouseEvent.CLICK, handleScrollUp, false, 0, true);
			downArrow.addEventListener(MouseEvent.CLICK, handleScrollDown, false, 0, true);
			txt.reportLink.buttonMode = true;
			txt.reportLink.addEventListener(MouseEvent.CLICK, handleReportLink, false, 0, true);
		}
		
		private function handleScrollUp( evt:MouseEvent ):void {
			var currentY:Number 	= txt.y;
			var targetY:Number 		= txt.y + _scrollIncrement;
			
			if(targetY  >= (scrollMask.y +20)){
				targetY = scrollMask.y + 20;
			}
			
			TweenMax.to(txt, 1, {y:targetY, ease:Expo.easeOut});
		}
		
		private function handleScrollDown( evt:MouseEvent ):void {
			var currentY:Number 	= txt.y;
			var targetY:Number 		= txt.y - _scrollIncrement;
			
			if(targetY < scrollMask.y - txt.height + scrollMask.height - 20){
				targetY = scrollMask.y - txt.height + scrollMask.height - 20;
			}
			
			TweenMax.to(txt, 1, {y:targetY, ease:Expo.easeOut});
		}
		
		
		private function handleClose( evt : MouseEvent ):void {
			controller.changeScreenInit("WALL_SCREEN");
		}
		
		private function handleReportLink( evt:MouseEvent):void{
			var targetURL:URLRequest = new URLRequest("mailto:info@wcb.gov.ns.ca");
			navigateToURL(targetURL, "_self"); 
		}
		
	}
}