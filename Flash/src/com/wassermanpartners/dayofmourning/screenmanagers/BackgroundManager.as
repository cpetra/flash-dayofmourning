package com.wassermanpartners.dayofmourning.screenmanagers
{
	import com.wassermanpartners.dayofmourning.ScreenController;
	import com.wassermanpartners.dayofmourning.ScreenModel;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.display.GradientType;
	import flash.events.Event;
	import flash.display.BitmapData;
	import flash.display.Bitmap;

	public class BackgroundManager extends MovieClip
	{
		private var _model			: ScreenModel;
		private var _controller		: ScreenController;
		private var _gradientType	: String;
		private var _matrix			: Matrix;
		private var _background		: Sprite;
		private var _vingette		: Sprite;
		private var _stageWidth		: Number;
		private var _stageHeight 	: Number;
		
		public function BackgroundManager( pModel:ScreenModel, pController:ScreenController )
		{
			super();
			_model = pModel;
			_controller = pController;
			
			this.addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
			
		}
		
		private function init( evt:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_model.addEventListener(Event.RESIZE, handleResize, false, 0, true);
			
			_gradientType = GradientType.RADIAL;
			_matrix = new Matrix();
			_matrix.createGradientBox(stage.stageWidth, stage.stageHeight, 0, 0, 0);
			
			var gColors:Array = [0x000000, 0x000000];
			var gAlphas:Array = [0, 0.4];
			var gRatio:Array = [165, 255];
			
			_vingette = new Sprite();
			_vingette.graphics.beginGradientFill(_gradientType, gColors, gAlphas, gRatio, _matrix);
			_vingette.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			_vingette.x = 0;
			_vingette.y = 0;
			
			_background = getBackgroundImage();
			this.addChild(_background);
			this.addChild(_vingette);
			
			handleResize();
		}
		
		private function handleResize( evt:Event = null ):void
		{
			_stageWidth = stage.stageWidth;
			_stageHeight = stage.stageHeight;
			
			_vingette.width = _stageWidth;
			_vingette.height = _stageHeight;
			_vingette.x = -(_stageWidth - 1280)/2;
			_vingette.y = -this.y-this.parent.y;
			
			
			_background.graphics.clear();
			_background.graphics.beginBitmapFill(new BackgroundTile(0, 0));
			_background.graphics.drawRect(0, 0, _stageWidth, _stageHeight);
			_background.graphics.endFill();
			
			_background.x = -(_stageWidth - 1280)/2;
			_background.y = -this.parent.y;
		}
		
		private function getBackgroundImage():Sprite
		{
			var backGroundSprite:Sprite = new  Sprite();
			backGroundSprite.graphics.beginBitmapFill(new BackgroundTile(0, 0));
			backGroundSprite.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			backGroundSprite.graphics.endFill();
			return backGroundSprite;
		}
		
	}
}