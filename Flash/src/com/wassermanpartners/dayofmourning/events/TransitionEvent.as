package com.wassermanpartners.dayofmourning.events
{
	import flash.events.Event;

	public class TransitionEvent extends Event
	{
		
		public static const TRANS_OUT		:String	= "transitionOut";
		public static const TRANS_IN	:String		= "transitionIn";
		
		private var _target				:String;
		
		public function TransitionEvent(
			pType:String,
			pTarget:String,
			pBubbles:Boolean=false,
			pCancelable:Boolean=false
		) {
			super(pType, pBubbles, pCancelable);
			_target 	= pTarget;
		}
		
		override public function clone():Event
		{
			return new TransitionEvent(
				type,
				_target,
				bubbles,
				cancelable
			);
		}
		
		public function get targetScreen():String		{ return _target; }
		
	}
}