package com.wassermanpartners.dayofmourning.events
{
	import flash.events.Event;

	public class DataLoadEvent extends Event
	{
		
		public static const INITIAL_LOAD_COMPLETE		:String	= "Initial_Load_Complete";
		public static const INITIAL_LOAD_FAIL			:String	= "Initial_Load_Failed";
		public static const DEDICATION_LOAD_COMPLETE	:String	= "Dedication_Load_Complete";
		public static const COMMENTS_LOAD_COMPLETE		:String	= "Comments_Load_Complete";
		
		private var _target				:String;
		
		public function DataLoadEvent(
			pType:String,
			pBubbles:Boolean=false,
			pCancelable:Boolean=false
		) {
			super(pType, pBubbles, pCancelable);
		}
		
		override public function clone():Event
		{
			return new DataLoadEvent(
				type,
				bubbles,
				cancelable
			);
		}
		
	}
}