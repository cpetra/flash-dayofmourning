package com.wassermanpartners.dayofmourning.events
{
	import flash.events.Event;

	public class FlowerEvent extends Event
	{
		public static const DROP_FLOWER		: String = "dropFlower";
		public static const UPDATE_FLOWER	: String = "updateFlower";
		
		public function FlowerEvent(
			pType:String,
			pBubbles:Boolean=false,
			pCancelable:Boolean=false
		) {
			super(pType, pBubbles, pCancelable);
		}
		
		override public function clone():Event
		{
			return new FlowerEvent(
				type,
				bubbles,
				cancelable
			);
		}
		
	}
}