package com.wassermanpartners.dayofmourning.events
{
	import flash.events.Event;

	public class HeadFlowerEvent extends Event
	{
		
		public static const SHOW_HEAD_FLOWER		:String	= "show_head_flower";
		public static const HIDE_HEAD_FLOWER		:String	= "hide_head_flower";
		public static const HOLD_HEAD_FLOWER		:String = "hold_head_flower";
		public static const HOLD_HEAD_OVERRIDE		: String = "hold_head_override";
		
		private var _headName				: String;
		private var _headFlowerID			: String;
		private var _holdHeadFlower			: Boolean;
		private var _holdHeadOverride		: Boolean;
		
		public function HeadFlowerEvent(
			pType:String,
			pHeadName:String,
			pHeadFlowerID:String,
			pHoldHeadFlower:Boolean = false,
			pHoldHeadOverride:Boolean = false,
			pBubbles:Boolean=false,
			pCancelable:Boolean=false
		) {
			super(pType, pBubbles, pCancelable);
			_headName 		= pHeadName;
			_headFlowerID 	= pHeadFlowerID;
			_holdHeadFlower	= pHoldHeadFlower;
			_holdHeadOverride= pHoldHeadOverride;
		}
		
		override public function clone():Event
		{
			return new HeadFlowerEvent(
				type,
				_headName,
				_headFlowerID,
				_holdHeadFlower,
				_holdHeadOverride,
				bubbles,
				cancelable
			);
		}
		
		public function get headName():String		{ return _headName; }
		public function get headFlowerID():String	{ return _headFlowerID; }
		public function get headFlowerHold():Boolean{ return _holdHeadFlower; }
		public function get headFlowerOverride():Boolean{ return _holdHeadOverride; }
		
	}
}