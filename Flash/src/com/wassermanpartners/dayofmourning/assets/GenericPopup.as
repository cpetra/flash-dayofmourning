package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;

	public class GenericPopup extends MovieClip
	{
		
		public var closeBtn			: GenericTextButton;
		
		public static var CLOSE_POPUP:String = "close_popup";
		
		
		public function GenericPopup()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			closeBtn.addEventListener(MouseEvent.CLICK, handleClosePopup, false, 0, true);
		}
		
		private function handleClosePopup( evt:MouseEvent ):void
		{
			dispatchEvent(new Event(CLOSE_POPUP));
		}
		
	}
}