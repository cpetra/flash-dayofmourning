package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;

	public class PageButton extends MovieClip
	{
		public var txt			: TextField;
		public var hit			: MovieClip;
		
		private var _strength	: Number	=	1;
		private var _glowColor	: Number 	= 0xD3A43E;
		private var _pageNum	: Number;
		private var _active		: Boolean	= false;
		
		public function PageButton()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			hit.buttonMode = true;
			hit.addEventListener(MouseEvent.MOUSE_OVER, handleOver, false, 0, true);
			hit.addEventListener(MouseEvent.MOUSE_OUT, handleOut, false, 0, true);
		}
		
		private function handleOver( evt : MouseEvent ):void {
			if(!_active){
				TweenMax.to(txt, 0.5, {tint:_glowColor, glowFilter:{color:_glowColor, alpha:1, stregnth:_strength, blurX:10, blurY:10}, ease:Quad.easeOut});
			}
		}
		
		private function handleOut( evt : MouseEvent ):void {
			if(!_active){
				TweenMax.to(txt, 0.5, {tint:0xd1d1d1, glowFilter:{color:_glowColor, alpha:0, strength:_strength, blurX:10, blurY:10}, ease:Quad.easeOut});
			}
		}
		
		public function set active( pActive:Boolean ):void
		{
			if(pActive){
				// highlight button
				_active = pActive;
				TweenMax.to(txt, 0.5, {tint:_glowColor, ease:Quad.easeOut});
			}else{
				// unhighlight button
				_active = pActive;
				TweenMax.to(txt, 0.5, {tint:0xd1d1d1, ease:Quad.easeOut});
			}
		}
		
		public function set buttonText( pText:String ):void
		{
			txt.text = pText;
			txt.autoSize = TextFieldAutoSize.LEFT;
			hit.width = txt.width;
		}
		
		public function get pageNum():Number
		{
			return _pageNum;
		}
		
		public function set pageNum( pNum:Number ):void
		{
			_pageNum = pNum;
		}
		
		public function halt():void
		{
			hit.buttonMode = false;
			hit.removeEventListener(MouseEvent.MOUSE_OVER, handleOver);
			hit.removeEventListener(MouseEvent.MOUSE_OUT, handleOut);
		}
		
	}
}