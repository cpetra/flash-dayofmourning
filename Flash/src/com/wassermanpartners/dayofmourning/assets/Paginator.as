package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import com.wassermanpartners.dayofmourning.assets.PageButton;
	import flash.events.MouseEvent;
	import flash.events.Event;

	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.display.Sprite;

	public class Paginator extends MovieClip
	{
		public static var PAGINATE_UPDATE : String = "PAGINATE_UPDATE";
		
		public var pagePrevious		: MovieClip;
		public var pageNext			: MovieClip;
		public var pageMask			: MovieClip;
		
		private var _pageContainer		: Sprite;
		private var _currentPage		: Number;
		private var _pagesArray			: Array;
		private var _activePage			: PageButton;
		private var _arrowsShowing		: Boolean = false;
		private var _paginateNum		: Number = 10;
		
		public function Paginator()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			_pagesArray = [];
			_pageContainer = new Sprite();
			pageMask.width = 1;
			addChild(_pageContainer);
			pagePrevious.visible = false;
			pageNext.visible = false;
			pagePrevious.buttonMode = true;
			pageNext.buttonMode = true;
		}
		
		private function getNextXPos():Number
		{
			if(_pagesArray.length == 0){
				return 0;//pagePrevious.x + pagePrevious.width + 5;
			}else{
				return _pagesArray[_pagesArray.length-1].x + _pagesArray[_pagesArray.length-1].width + 5;
			}
		}
		
		public function set numPages( pNumPages:Number ):void
		{
			if(pNumPages > 0){
				// add pages
				_pagesArray = [];
				for(var i:int = 0; i<pNumPages; i++){
					var newPageBtn:PageButton = new PageButton();
					newPageBtn.buttonText = (i+1).toString();
					newPageBtn.pageNum = i;
					newPageBtn.x = getNextXPos();
					newPageBtn.y = pagePrevious.y - 2;
					newPageBtn.addEventListener(MouseEvent.CLICK, handleClickPage, false, 0, true);
					_pageContainer.addChild(newPageBtn);
					_pagesArray.push(newPageBtn);
				}
				
				
				// check to see if we have more than paginateNum.  if so then setup side to side scrolling
				if(_pagesArray.length > _paginateNum){
					_arrowsShowing = true;
					_pageContainer.mask = pageMask;
					pageMask.width = _pagesArray[_paginateNum-1].x + _pagesArray[_paginateNum-1].width;
					pageMask.x = pagePrevious.x + pagePrevious.width + 5;
					_pageContainer.x = pagePrevious.x + pagePrevious.width + 5;
					pageNext.x = pageMask.x + pageMask.width + 8;
					pageNext.visible = true;
					pagePrevious.visible = true;
					pageNext.addEventListener(MouseEvent.CLICK, handlePageNext, false, 0, true);
					pagePrevious.addEventListener(MouseEvent.CLICK, handlePagePrevious, false, 0, true);
				}else{
					_arrowsShowing = false;
					pageNext.visible = false;
					pagePrevious.visible = false;
					pageNext.removeEventListener(MouseEvent.CLICK, handlePageNext);
					pagePrevious.removeEventListener(MouseEvent.CLICK, handlePagePrevious);
					_pageContainer.x = pagePrevious.x + pagePrevious.width + 5;
					pageNext.x = _pageContainer.x + _pageContainer.width + 5;
				}
				
				
			}else{
				// clear pages
				if(_pagesArray.length > 0){
					for(var k:int = 0; k<_pagesArray.length; k++){
						_pageContainer.mask = null;
						pageMask.width = 1;
						_pageContainer.removeChild(_pagesArray[k]);
						_pagesArray[k].removeEventListener(MouseEvent.CLICK, handleClickPage);
						_pagesArray[k].halt();
					}
				}
			}
		}
		
		private function handlePageNext( evt:MouseEvent ):void
		{
			var targetX:Number = _pageContainer.x - (pageMask.width-15);
			if(targetX < pagePrevious.x + pagePrevious.width + 5 - (_pageContainer.width-pageMask.width)){
				targetX = pagePrevious.x + pagePrevious.width + 5 - (_pageContainer.width-pageMask.width);
			}
			
			TweenMax.to(_pageContainer, 0.5, {x:targetX, ease:Expo.easeOut});
		}
		
		private function handlePagePrevious( evt:MouseEvent ):void
		{
			var targetX:Number = _pageContainer.x + (pageMask.width - 15);
			if(targetX > pagePrevious.x + pagePrevious.width + 5){
				targetX = pagePrevious.x + pagePrevious.width + 5;
			}
			
			TweenMax.to(_pageContainer, 0.5, {x:targetX, ease:Expo.easeOut});
		}
		
		private function handleClickPage( evt:MouseEvent ):void
		{
			_activePage.active = false;
			_activePage = evt.currentTarget as PageButton;
			_activePage.active = true;
			_currentPage = _activePage.pageNum;
			
			dispatchEvent(new Event(Paginator.PAGINATE_UPDATE));
		}
		
		public function get currentPage():Number
		{
			return _currentPage;
		}
		
		public function set currentPage( pPage:Number ):void
		{
			if(_activePage){
				_activePage.active = false;
			}
			_activePage = _pagesArray[pPage];
			_activePage.active = true;
			_currentPage = _activePage.pageNum;
			
			if(_arrowsShowing){
				if(_currentPage > _pagesArray.length/2){
					// position on right
					_pageContainer.x = -_activePage.x + pagePrevious.width + 5 + pageMask.width - _activePage.width;
				}else{
					//position on left
					_pageContainer.x = -_activePage.x + pagePrevious.width + 5;
				}
			}
		}
		
		public function get paginatorWidth():Number
		{
			if(_arrowsShowing){
				return pagePrevious.width + 5 + pageMask.width + 8  + pageNext.width;
			}else{
				return this.width - pagePrevious.width;
			}
		}
	}
}