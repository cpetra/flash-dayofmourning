package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;

	public class GenericTextButton extends MovieClip
	{
		public var txt	: MovieClip;
		public var hit	: MovieClip;
		
		private var _txtColor:Number = 0xffffff;
		private var _txtOverColor:Number = 0x000000;
		private var _strength:Number	=	1;
		private var _glowColor:Number = 0xD3A43E;
		
		public function GenericTextButton()
		{
			super();
			
			init();
		}
		
		private function init():void {			
			hit.buttonMode = true;
			hit.addEventListener(MouseEvent.MOUSE_OVER, handleOver, false, 0, true);
			hit.addEventListener(MouseEvent.MOUSE_OUT, handleOut, false, 0, true);
		}
		
		private function handleOver( evt : MouseEvent ):void {
			//TweenMax.to(txt, 0.5, {tint:_txtOverColor, glowFilter:{color:_glowColor, alpha:1, blurX:30, blurY:30}, ease:Quad.easeOut});
			TweenMax.to(txt, 0.5, { glowFilter:{color:_glowColor, alpha:1, stregnth:_strength, blurX:10, blurY:10}, ease:Quad.easeOut});
		}
		
		private function handleOut( evt : MouseEvent ):void {
			//TweenMax.to(txt, 0.5, {tint:_txtColor, glowFilter:{color:_glowColor, alpha:0, blurX:10, blurY:10}, ease:Quad.easeOut});
			TweenMax.to(txt, 0.5, { glowFilter:{color:_glowColor, alpha:0, strength:_strength, blurX:10, blurY:10}, ease:Quad.easeOut});
		}
		
		public function set blurStrength ( pStrength:Number ):void {
			_strength = pStrength;
		}
		
		public function set glowColor ( pGlowColor:Number ):void
		{
			_glowColor = pGlowColor;
		}
		
		override public function set enabled ( pEnabled:Boolean ):void
		{
			if(pEnabled){
				hit.buttonMode = true;
				hit.addEventListener(MouseEvent.MOUSE_OVER, handleOver, false, 0, true);
				hit.addEventListener(MouseEvent.MOUSE_OUT, handleOut, false, 0, true);
				this.alpha = 1;
			}else{
				hit.buttonMode = false;
				hit.removeEventListener(MouseEvent.MOUSE_OVER, handleOver);
				hit.removeEventListener(MouseEvent.MOUSE_OUT, handleOut);
				this.alpha = 0.5;
			}
		}
		
	}
}