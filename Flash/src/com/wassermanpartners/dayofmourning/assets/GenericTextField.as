package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.events.FocusEvent;
	import flash.filters.GlowFilter;
	
	import com.greensock.TweenMax;
	
	import com.gskinner.StringUtils;

	public class GenericTextField extends MovieClip
	{
		public var txt		: TextField;
		public var bg		: MovieClip;
		public var validArrow: MovieClip;
		
		private var _label	: String = "txt";
		private var _glowColor	: Number = 0xD3A43E;
		
		public function GenericTextField()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			validArrow.alpha = 0;
			txt.text = _label;
			txt.addEventListener(FocusEvent.FOCUS_IN, handleFocusIn, false, 0, true);
			txt.addEventListener(FocusEvent.FOCUS_OUT, handleFocusOut, false, 0, true);
		}
		
		/*
		FFFFFF  OOOO   CCCCC  UU   UU  SSSSS 	EEEEEEE V     V EEEEEEE NN  NN TTTTTT  SSSSS 
		FF     OO  OO CC   CC UU   UU SS     	EE      V     V EE      NNN NN   TT   SS     
		FFFF   OO  OO CC      UU   UU  SSSS  	EEEE     V   V  EEEE    NNNNNN   TT    SSSS  
		FF     OO  OO CC   CC UU   UU     SS 	EE        V V   EE      NN NNN   TT       SS 
		FF      OOOO   CCCCC   UUUUU  SSSSS  	EEEEEEE    V    EEEEEEE NN  NN   TT   SSSSS  
		*/
		
		
		
		private function handleFocusIn( evt:FocusEvent ):void
		{
			TweenMax.to(bg, 0.5, {glowFilter:{color:_glowColor, alpha:1, blurX:10, blurY:10}});
			validArrow.alpha = 0;
			TweenMax.killDelayedCallsTo(hideArrow);
			if(txt.text == _label){
				txt.text = "";
			}
		}
		
		private function handleFocusOut( evt:FocusEvent ):void
		{
			TweenMax.to(bg, 0.5, {glowFilter:{color:_glowColor, alpha:0, blurX:10, blurY:10, remove:true}});
			
			if(StringUtils.removeExtraWhitespace(txt.text) == ""){
				txt.text = _label;
			}
		}
		
		private function hideArrow():void
		{
			TweenMax.to(validArrow, 0.5, {alpha:0, delay:2});
		}
		
		public function get fieldText():String
		{
			return txt.text;
		}
		
		public function set fieldText( pText:String ):void
		{
			txt.text = pText;
		}
		
		public function set fieldLabel( pLabel:String ):void
		{
			_label = pLabel;
			txt.text = pLabel;
		}
		
		public function get fieldLabel():String 
		{
			return _label;
		}
		
		public function clear():void
		{
			txt.text = _label;
			TweenMax.to(bg, 0.5, {glowFilter:{color:_glowColor, alpha:0, blurX:10, blurY:10, remove:true}});
		}
		
		public function set valid( pValid:Boolean ):void
		{
			if(!pValid){
				TweenMax.to(validArrow, 0.5, {alpha:1});
				TweenMax.delayedCall(2, hideArrow);
				TweenMax.to(bg, 0.5, {glowFilter:{color:0xFF0000, alpha:1, blurX:10, blurY:10}});
			}else{
				TweenMax.to(bg, 0.6, {glowFilter:{color:0x99cc00, alpha:1, blurX:10, blurY:10, remove:true},repeat:1, yoyo:true});
			}
		}
		
		public function set glowColor( pGlowColor:Number ):void
		{
			_glowColor = pGlowColor;
		}
		
		public function set maxChars( pMax:Number ):void
		{
			txt.maxChars = pMax;
		}
		
		public function get maxChars():Number
		{
			return txt.maxChars;
		}
		
		public function set restrict( pRestrict:String ):void
		{
			txt.restrict = pRestrict;
		}
		
		public function get restrict():String
		{
			return txt.restrict;
		}
	
	}
}