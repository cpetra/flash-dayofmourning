package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.events.ProgressEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;

	public class SubmitStatusScreen extends MovieClip
	{
		public var done_btn					: GenericTextButton;
		public var status_txt				: TextField;
		public var progressBar				: MovieClip;
		public var progressBarBack			: MovieClip;
		public var facade					: MovieClip;
		public var uploadLabel				: MovieClip;
		public var statusLabel				: MovieClip;
		public var bg						: MovieClip;
		public var success_msg				: MovieClip;
		
		private var _progressBarWidth		: Number;
		
		public function SubmitStatusScreen()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			this.alpha = 0;
			this.visible = false;
			
			done_btn.visible = false;
			success_msg.visible = false;
			_progressBarWidth = progressBar.width;
		}
		
		/*
		PPPPPP  UU   UU BBBBBB  LL      IIIIII  CCCCC          MM   MM EEEEEEE TTTTTT HH   HH  OOOO  DDDDDD   SSSSS 
		PP   PP UU   UU BB   BB LL        II   CC   CC         MMM MMM EE        TT   HH   HH OO  OO DD   DD SS     
		PPPPPP  UU   UU BBBBBB  LL        II   CC              MMMMMMM EEEE      TT   HHHHHHH OO  OO DD   DD  SSSS  
		PP      UU   UU BB   BB LL        II   CC   CC         MM M MM EE        TT   HH   HH OO  OO DD   DD     SS 
		PP       UUUUU  BBBBBB  LLLLLLL IIIIII  CCCCC          MM   MM EEEEEEE   TT   HH   HH  OOOO  DDDDDD  SSSSS  
		*/
		
		public function setStatus( pStatus:String ):void
		{
			status_txt.text = pStatus;
		}
		
		public function handleOpenScreen():void
		{
			success_msg.visible = false;
			TweenMax.to(this, 0.5, {autoAlpha:1});
		}
		
		public function handleHideScreen( evt:MouseEvent = null ):void
		{
			TweenMax.to(this, 0.5, {autoAlpha:0 });
		}
		
		public function onUploadProgress( evt:ProgressEvent ):void
		{
			var perc:Number = evt.bytesLoaded/evt.bytesTotal;
			progressBar.width = perc*_progressBarWidth;
			status_txt.text = Math.round(perc*100).toString() + "%";
			status_txt.autoSize = TextFieldAutoSize.RIGHT;
			statusLabel.x = status_txt.x - statusLabel.width - 10;
		}
		
		public function handleCompleteLoaded():void
		{
			done_btn.visible = true;
		}
		
		public function set showPreload( pShow:Boolean ):void
		{
			progressBarBack.visible = pShow;
			progressBar.visible = pShow;
			uploadLabel.visible = pShow;
		}
		
		public function set showStatus( pShow:Boolean ):void
		{
			statusLabel.visible = pShow;
			status_txt.visible = pShow;
		}
		
		public function prepForAllWorkers():void
		{
			this.showPreload = false;
			this.showStatus = false;
		}
		
		public function showSuccessStatus():void
		{
			success_msg.visible = true;
		}
		
	}
}