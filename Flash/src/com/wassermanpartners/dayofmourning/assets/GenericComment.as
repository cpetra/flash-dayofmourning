package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;

	public class GenericComment extends MovieClip
	{
		public var name_txt		: TextField;
		public var date_txt		: TextField;
		public var comment_txt	: TextField;
		public var comment_bg	: MovieClip;
		public var divider		: MovieClip;
		public var comment_icon	: MovieClip;
		
		private var _highlighted: Boolean = false;
		
		public function GenericComment()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			this.addEventListener(MouseEvent.MOUSE_OVER, handleOverComment, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OUT, handleOutComment, false, 0, true);
		}
		
		private function handleOverComment( evt:MouseEvent ):void
		{
			if(_highlighted){
				TweenMax.to(comment_bg, 0.5, {alpha:0.15});
			}else{
				TweenMax.to(comment_bg, 0.5, {alpha:0.05});
			}
			
		}
		
		private function handleOutComment( evt:MouseEvent ):void
		{
			if(_highlighted){
				TweenMax.to(comment_bg, 0.5, {alpha:0.10});
			}else{
				TweenMax.to(comment_bg, 0.5, {alpha:0});
			}
		}
		
		/*
		PPPPPP  UU   UU BBBBBB  LL      IIIIII  CCCCC          MM   MM EEEEEEE TTTTTT HH   HH  OOOO  DDDDDD   SSSSS 
		PP   PP UU   UU BB   BB LL        II   CC   CC         MMM MMM EE        TT   HH   HH OO  OO DD   DD SS     
		PPPPPP  UU   UU BBBBBB  LL        II   CC              MMMMMMM EEEE      TT   HHHHHHH OO  OO DD   DD  SSSS  
		PP      UU   UU BB   BB LL        II   CC   CC         MM M MM EE        TT   HH   HH OO  OO DD   DD     SS 
		PP       UUUUU  BBBBBB  LLLLLLL IIIIII  CCCCC          MM   MM EEEEEEE   TT   HH   HH  OOOO  DDDDDD  SSSSS  
		*/
		
		public function set commentName( pName:String ):void
		{
			name_txt.text = pName;
			name_txt.autoSize = TextFieldAutoSize.LEFT;
			divider.x = name_txt.x + name_txt.width + 5;
			date_txt.x = divider.x + divider.width + 5;
		}
		
		public function set commentDate( pDate:String ):void
		{
			date_txt.text = pDate;
			date_txt.autoSize = TextFieldAutoSize.LEFT;
		}
		
		public function set commentText( pComment:String ):void
		{
			comment_txt.text = pComment;
			comment_txt.autoSize = TextFieldAutoSize.LEFT;
			comment_bg.height = comment_txt.x + comment_txt.height + 10;
		}
		
		public function set highlight( pHighlight:Boolean ):void
		{
			_highlighted = pHighlight;
			if(pHighlight){
				comment_bg.alpha = 0.1;
			}else{
				comment_bg.alpha = 0;
			}
		}
		
		public function halt():void
		{
			this.removeEventListener(MouseEvent.MOUSE_OVER, handleOverComment);
			this.removeEventListener(MouseEvent.MOUSE_OUT, handleOutComment);
		}
	}
}