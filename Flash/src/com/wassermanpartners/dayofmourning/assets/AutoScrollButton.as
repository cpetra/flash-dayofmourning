package com.wassermanpartners.dayofmourning.assets
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;

	public class AutoScrollButton extends MovieClip
	{		
		private var _autoScrolling		: Boolean 	= true;
		private var _strength			: Number	=	1;
		
		public function AutoScrollButton()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			this.gotoAndStop(1);
			this.addEventListener(MouseEvent.CLICK, handleClick, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OVER, handleOver, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OUT, handleOut, false, 0, true);
		}
		
		private function handleClick( evt:MouseEvent ):void
		{
			if(_autoScrolling){
				this.gotoAndPlay(2);
				_autoScrolling = false;
				handleOver();
			}else{
				this.gotoAndPlay(1);
				_autoScrolling = true;
				handleOver();
			}
		}
		
		private function handleOver( evt:MouseEvent = null ):void
		{
			if(_autoScrolling){
				TweenMax.to(this, 0.5, { glowFilter:{color:0xFF00000, alpha:1, stregnth:_strength, blurX:15, blurY:15}, ease:Quad.easeOut});
			}else{
				TweenMax.to(this, 0.5, { glowFilter:{color:0x006600, alpha:1, stregnth:_strength, blurX:15, blurY:15}, ease:Quad.easeOut});
			}
		}
		
		private function handleOut( evt:MouseEvent ):void
		{
			TweenMax.to(this, 0.5, { glowFilter:{alpha:0}, ease:Quad.easeOut});
		}
		
		public function get autoscrolling():Boolean
		{
			return _autoScrolling;
		}
		
	}
}