package com.wassermanpartners.dayofmourning
{
	import com.wassermanpartners.dayofmourning.events.TransitionEvent;
	import com.wassermanpartners.dayofmourning.ScreenModel;
	import com.wassermanpartners.dayofmourning.ScreenController;
	
	import flash.events.Event;
	
	public class CompositeView extends ComponentView
	{
		
		private var aChildren		: Array;
		
		public function CompositeView(pModel:ScreenModel, pController:ScreenController=null)
		{
			super(pModel, pController);
			this.aChildren		= new Array();
		}
		
		override public function add( c:ComponentView ):void {
			aChildren.push(c);
		}
		
		override public function transitionInScreen(evt:TransitionEvent=null):void {
			for each (var c:ComponentView in aChildren){
				c.transitionInScreen(evt);
			}
		}
		
		override public function transitionOutScreen(evt:TransitionEvent=null):void {
			for each (var c:ComponentView in aChildren){
				c.transitionOutScreen(evt);
			}
		}
		
	}
}