<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("./model/dedication.php");
$personId = $_GET['personId'];
$all = getDedicationsByPerson($personId); 
header('Content-Type: text/xml');
print '<?xml version="1.0" encoding="utf-8"?>'."\n";
print '<dedications total="'.count($all).'">'."\n";
foreach($all as $dedication){
    print '<dedication comments ="'.$dedication['count'].'">'."\n";
    print '<id>'.$dedication['id'].'</id>'."\n";
    print '<description>'.str_replace("&", "&amp;", $dedication['description']).'</description>'."\n";
    print '<flowerID>'.$dedication['flowerID'].'</flowerID>'."\n";
    print '<image>'.$dedication['image'].'</image>'."\n";
    print '</dedication>'."\n";
}
print '</dedications>'."\n";

