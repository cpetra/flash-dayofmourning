<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("./model/dedication.php");

$senderFirstName = $_POST['senderFirst'];
$senderLastName = $_POST['senderLast'];
$senderEmail = $_POST['senderEmail'];
$receiverFirstName = $_POST['recFirst'];
$receiverLastName = $_POST['recLast'];
$receiverEmail = $_POST['recEmail'];
$dedicationId = $_POST['dedicationId'];

if(!is_numeric($dedicationId))
    exit;
$dedication = getDedicationById($dedicationId);


$to = $receiverEmail;
$subject = 'Day of Mourning';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: info@safeandgreen.ca\r\nReply-To: info@safeandgreen.ca". "\r\n";



$message = "
<html>

<body>
<div style=\"background-image:src('http://safeandgreen.ca/images/tile.png')\">
<h2>".$dedication['name']."</h2>
<hr />

The following dedication has been sent to you by $senderFirstName $senderLastName

You can visit the dedication on the Day of Mourning dedication wall at
http://dayofmourning.bc.ca/dedicationId=$dedicationId
<hr />".
$dedication['description']."
</div>

</body></html>";


//send the email
$mail_sent = @mail( $to, $subject, $message, $headers );
//if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
echo $mail_sent ? "Mail sent" : "Mail failed";
