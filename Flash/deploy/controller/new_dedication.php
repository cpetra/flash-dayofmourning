<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("./model/dedication.php");
require_once("./model/person.php");
$firstName = $_POST['first_name'];
$lastName = $_POST['last_name'];
$middleName = $_POST['middle_name'];
$toAll = $_POST['all'];
$flowerId = $_POST['flower'];
$dedication = $_POST['dedication'];
$description = $_POST['description'];
$personId = $_POST['personId'];
$ip = $_SERVER['REMOTE_ADDR'];

if(is_numeric($personId)){
    $personId = $personId;
}else{
    $personId = createPerson($firstName, $middleName, $lastName);
}
if(is_file($_FILES['user_pic']['tmp_name'])){
    $target = "./images/uploads/";
    $target_path = $target.basename( $_FILES['user_pic']['name']);

    $fileName = md5($_FILES['user_pic']['name'].time().rand(0,99999)).".jpg";
    $fileName = $fileName;
    move_uploaded_file($_FILES['user_pic']['tmp_name'], $target_path) ;

    processImage($target_path, $fileName, $target);
}

createDedication($description, $flowerId, 3, $fileName, $ip, $personId);
echo "success";

function processImage($target_path, $fileName, $target){
    $src = $target_path;
    $fileName = $target.$fileName;
    $imageInfo = getimagesize($target_path);
    $originalWidth = $imageInfo[0];
    $originalHeight = $imageInfo[1];
    $type = $imageInfo[2];
    if($originalWidth > 170){
        $finalWidth = 170;
        $finalHeight = ($originalHeight / $originalWidth) * 170;
    }else{
        $finalWidth = 170;
        $finalHeight = $originalHeight;
    }
    if ($type == 1) {
        $srcImage = imagecreatefromgif ( $src );
    } elseif ($type == 2) {
        $srcImage = imagecreatefromjpeg ( $src );
    } elseif ($type == 3) {
        $srcImage = imagecreatefrompng ( $src );
    } else {
        unlink ( $src );
        return;
    }
    $dstImg = imagecreatetruecolor($finalWidth, $finalHeight);
    imagecopyresampled ( $dstImg, $srcImage, 0, 0, 0, 0, $finalWidth, $finalHeight, $originalWidth, $originalHeight );
    imagejpeg($dstImg, $fileName, 100);
    imagedestroy($srcImage);
    imagedestroy($dstImg);
    unset($srcImage);
    unset($dstImg);
    unlink($src);
}
