<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("./model/person.php");
$all = getAll();
header('Content-Type: text/xml');
print '<?xml version="1.0" encoding="utf-8"?>'."\n";
print '<all>'."\n";
foreach($all as $person){
    print '<person>'."\n";
    print '<id>'.$person['id'].'</id>'."\n";
    print '<first_name>'.str_replace("&", "&amp;", $person['first_name']).'</first_name>'."\n";
    print '<middle_name>'.str_replace("&", "&amp;", $person['middle_name']).'</middle_name>'."\n";
    print '<last_name>'.str_replace("&", "&amp;", $person['last_name']).'</last_name>'."\n";
    print '<flowerID>'.str_replace("&", "&amp;", $person['flowerID']).'</flowerID>'."\n";
    print '</person>'."\n";
}
print '</all>'."\n";

