<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("./model/comment.php");
$dedicationId = $_GET['dedicationId'];
$all = getCommentsByDedication($dedicationId);
header('Content-Type: text/xml');
print '<?xml version="1.0" encoding="utf-8"?>'."\n";
print '<comments total="'.count($all).'">'."\n";
foreach($all as $comment){
    print '<comment>'."\n";
    print '<id>'.$comment['id'].'</id>'."\n";
    print '<name>'.$comment['name'].'</name>'."\n";
    print '<description>'.str_replace("&", "&amp;", $comment['description']).'</description>'."\n";
    print '<submitted>'.date("F j,o",strtotime($comment['created_at'])).'</submitted>'."\n";
    print '</comment>'."\n";
}
print '</comments>'."\n";

