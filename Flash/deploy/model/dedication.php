<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("database.php");
/**
 *
 * @return <array> array of clients from database.
 */

function getDedicationsByPerson($personId){
    $query = "select d.id,d.description,d.flowerID,d.image,count(c.id) as count from dedication as d left join comment as c on d.id=c.dedication_id
        where person_id = ".mysql_real_escape_string($personId)." group by d.id,d.description,d.flowerID,d.image ";
    $result = mysql_query($query);
    $dedications = array(); //var_dump($query);die();
    while($row = mysql_fetch_assoc($result)){ //var_dump($row);die();
        $eachDedication = array();
        $eachDedication['id'] = $row['id'];
        $eachDedication['description'] = $row['description'];
        $eachDedication['flowerID'] = $row['flowerID'];
        $eachDedication['image'] = $row['image'];
        $eachDedication['count'] = $row['count'];
        $dedications[] = $eachDedication;
    }


    return $dedications;
}
function getDedications($moderated, $limit){
    global $count; 
    $query = "select SQL_CALC_FOUND_ROWS dedication.id, first_name, middle_name, last_name,submitted,ip from dedication join person on person.id = dedication.person_id
        where dedication.moderated =".mysql_real_escape_string($moderated) ." order by dedication.id desc $limit "; 
    $result = mysql_query($query) ;
    $sql = "SELECT FOUND_ROWS()";
    $count = mysql_result(mysql_query($sql),0,0);
    $dedications = array();
    while($row = mysql_fetch_assoc($result)){
        $eachDedication = array();
        $eachDedication['id'] = $row['id'];
        $eachDedication['first_name'] = $row['first_name'];
        $eachDedication['last_name'] = $row['last_name'];
        $eachDedication['middle_name'] = $row['middle_name'];
        $eachDedication['submitted'] = $row['submitted'];
        $eachDedication['ip'] = $row['ip'];
        $dedications[] = $eachDedication;
    }
    return $dedications;
}
function createDedication($description, $flowerId, $moderated, $image, $ip, $personId){
    $query = "insert into dedication values(null ,'".mysql_real_escape_string($description)."','".mysql_real_escape_string($flowerId)."',3,NOW(),'".$image."','".$ip."',$personId) ";
    $result = mysql_query($query) or die($query." ".mysql_error());
}
function getDedicationById($id){
    $query = "select d.id as id,d.description,d.flowerID,d.image,p.first_name,p.last_name,p.middle_name,p.id as person_id, d.submitted, d.ip
        from dedication as d join person as p on d.person_id = p.id where d.id = $id "; //var_dump($query);die();
    $result = mysql_query($query);
    $dedication = array();
    while($row = mysql_fetch_assoc($result)){
        $dedication['id'] = $row['id'];
        $dedication['description'] = $row['description'];
        $dedication['flowerID'] = $row['flowerID'];
        $dedication['image'] = $row['image'];
        $dedication['name'] = $row['first_name'].$row['middle_name'].$row['last_name'];
        $dedication['person_id'] = $row['person_id'];
        $dedication['submitted'] = $row['submitted'];
        $dedication['ip'] = $row['ip'];
    }
    return $dedication;
}
function updateDescription($description, $id){
    $query = "update dedication set description = '".mysql_real_escape_string($description)."', moderated = 1 where id = $id "; //var_dump($query);die();
    mysql_query($query);
}
function deleteDedication($dedicationId){
    $query = "delete from dedication where id = $dedicationId ";
    mysql_query($query);
}

