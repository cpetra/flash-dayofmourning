<?php
defined( '_dom' ) or die( 'Restricted access' );
require_once("database.php");

/**
 * This function checks if username/password supplied by user are correct.
 * @param <string> $userName - username entered by the user.
 * @param <string> $password - password entered by the user.
 * @return <depends> if username/password combination correct then we will return array otherwise false.
 */
function checkPassword($userName,$password){
    // Use mysql_real_escape_string to escape anything entered by user to prevent sql injection.
    $query = "select * from user where username='".mysql_real_escape_string($userName)."'";
    $result = mysql_query($query) or die ("Could not query for user: ".mysql_error());
    $row = mysql_fetch_assoc($result);

    $pw = $row["password"];
   
    if (sha1($password) == $pw)
    {
        return $row;
        
    }
    else{
            return false;
            
    }

    return false;
}
